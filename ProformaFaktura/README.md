# Proforma faktura

* Proforma využívá stejné schema jako [běžná faktura](https://bitbucket.org/tixik/pohodaexamples/src/master/FakturaBezVazbyNaZalohovku/README.md). Liší se pouze typem.
* [Rozdíly mezi CZ běžnou fakturou a proformou](https://bitbucket.org/tixik/pohodaexamples/src/master/ProformaFaktura/FakturaVSProformaCZ.png)
* [Rozdíly mezi SK běžnou fakturou a proformou](https://bitbucket.org/tixik/pohodaexamples/src/master/ProformaFaktura/FakturaVSProformaSK.png) 
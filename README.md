# Příklady pro import dokladů do Pohody

[Stažení zkušební Pohody (pro testování)](https://www.stormware.cz/pohoda/start/) - Bez registrace -> Stáhnout ihned

## Originální dokumentace - příklady a XSD schémata

* Veškeré XML dokumenty musí mít v tagu `dat:dataPack` vyplněn parametr `ico`, jehož hodnota odpovídá nastavenému IČ u účetní jednotky.
* Kódování Windows-1250 

### Faktury všech typů

[Dokumentace](https://www.stormware.cz/pohoda/xml/dokladyimport/#Faktury)

* Faktura (invoiceType = issuedInvoice)
* Proforma faktura (invoiceType = issuedProformaInvoice)
* Storno zálohové faktury (invoiceType = issuedProformaInvoice)
* Dobropis (invoiceType = issuedCreditNotice)

### Interní doklady (DDOPP)

[Dokumentace](https://www.stormware.cz/pohoda/xml/dokladyimport/#InterniDoklady)


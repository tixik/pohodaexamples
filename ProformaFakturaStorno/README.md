# Storno proforma faktury

* Storno využívá stejné schema jako [běžná faktura](https://bitbucket.org/tixik/pohodaexamples/src/master/FakturaBezVazbyNaZalohovku/README.md). Liší se pouze typem.
* Doklad musí mít svůj vlastní záznam s vlastním číslem. Pokud ve Variu vlastní číslo nemá, můžeme vymyslet číselnou řadu a připravím XML s automatickým generováním čísla dokladu. 
* Uvádí se vazba na původní doklad, který se importem automaticky označí jako stornovaný.
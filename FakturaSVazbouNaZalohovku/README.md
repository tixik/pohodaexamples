# Faktura

* V podstatě stejné jako [běžná faktura](https://bitbucket.org/tixik/pohodaexamples/src/master/FakturaBezVazbyNaZalohovku/README.md)
* Některé indormace jsou vynachané, protože se natáhnou ze zálohovky
* Přidává se tag `<inv:invoiceAdvancePaymentItem>`, který zařídí spárování se zálohovkou.
* SK faktury budeme importovat s ručním odpočtem zálohy. Tzn., že u faktury bude vedena hodnota odečítané zálohy, ale nebude mít vazbu na zálohovku. Tuto vazbu budu doplňovat scriptem přímo v DB. Oproti CZ faktuře tedy zrušíme tag `<inv:sourceDocument>` v `<inv:invoiceAdvancePaymentItem>` 
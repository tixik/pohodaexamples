# Faktura

* Faktury jsou importovány bez vazby na adresář.
* Faktury jsou importovány bez vazby na skladovou položku. Položky jsou pouze textové.
* **CZ Faktura**: v ukázkové faktuře jsou tři položky. Množství u každé položky jsem dal stejné, jako je výše jejich DPH. Nezadává se procentní sazba DPH, ale číselníkem v položce `rateVAT`. V příkladu 10%/15%/21% (third/low/high). Číselník viz níže.
* **SK Faktura**: U zahraničních dokladů se používají tzv. "historické" rateVAT, které umožňují zadat vlastní sazbu DPH.    
* [Dokumentace](https://www.stormware.cz/pohoda/xml/dokladyimport/#Faktury)
* [XSD](https://www.stormware.cz/xml/schema/version_2/invoice.xsd)

### paymentType

```
<xsd:enumeration value="draft">
    <xsd:annotation>
        <xsd:documentation>Příkazem.</xsd:documentation>
    </xsd:annotation>
</xsd:enumeration>
<xsd:enumeration value="cash">
    <xsd:annotation>
        <xsd:documentation>Hotově.</xsd:documentation>
    </xsd:annotation>
</xsd:enumeration>
<xsd:enumeration value="postal">
    <xsd:annotation>
        <xsd:documentation>Složenkou.</xsd:documentation>
    </xsd:annotation>
</xsd:enumeration>
<xsd:enumeration value="delivery">
    <xsd:annotation>
        <xsd:documentation>Dobírka.</xsd:documentation>
    </xsd:annotation>
</xsd:enumeration>
<xsd:enumeration value="creditcard">
    <xsd:annotation>
        <xsd:documentation>Platební kartou.</xsd:documentation>
    </xsd:annotation>
</xsd:enumeration>
<xsd:enumeration value="advance">
    <xsd:annotation>
        <xsd:documentation>Zálohová faktura.</xsd:documentation>
    </xsd:annotation>
</xsd:enumeration>
<xsd:enumeration value="encashment">
    <xsd:annotation>
        <xsd:documentation>Inkasem.</xsd:documentation>
    </xsd:annotation>
</xsd:enumeration>
<xsd:enumeration value="cheque">
    <xsd:annotation>
        <xsd:documentation>Šekem.</xsd:documentation>
    </xsd:annotation>
</xsd:enumeration>
<xsd:enumeration value="compensation">
    <xsd:annotation>
        <xsd:documentation>Zápočtem.</xsd:documentation>
    </xsd:annotation>
</xsd:enumeration>
```

### vatRateType
```
<xsd:simpleType name="vatRateType">
	<xsd:annotation>
		<xsd:documentation>Výčet možných sazeb DPH.</xsd:documentation>
	</xsd:annotation>
	<xsd:restriction base="xsd:string">
		<xsd:enumeration value="none">
			<xsd:annotation>
				<xsd:documentation>Bez DPH.</xsd:documentation>
			</xsd:annotation>
		</xsd:enumeration>
		<xsd:enumeration value="high">
			<xsd:annotation>
				<xsd:documentation>Základní sazba.</xsd:documentation>
			</xsd:annotation>
		</xsd:enumeration>
		<xsd:enumeration value="low">
			<xsd:annotation>
				<xsd:documentation>Snížená sazba.</xsd:documentation>
			</xsd:annotation>
		</xsd:enumeration>
		<xsd:enumeration value="third">
			<xsd:annotation>
				<xsd:documentation>3. sazba (pouze SK verze).</xsd:documentation>
			</xsd:annotation>
		</xsd:enumeration>
		<xsd:enumeration value="historyHigh">
			<xsd:annotation>
				<xsd:documentation>Historická základní sazba.</xsd:documentation>
			</xsd:annotation>
		</xsd:enumeration>
		<xsd:enumeration value="historyLow">
			<xsd:annotation>
				<xsd:documentation>Historická snížená sazba.</xsd:documentation>
			</xsd:annotation>
		</xsd:enumeration>		
		<xsd:enumeration value="historyThird">
			<xsd:annotation>
				<xsd:documentation>Historická 3. sazba (pouze SK verze).</xsd:documentation>
			</xsd:annotation>
		</xsd:enumeration>
	</xsd:restriction>
</xsd:simpleType>
```